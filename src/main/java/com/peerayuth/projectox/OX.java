/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.projectox;

import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class OX {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCal();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCal() {
        System.out.println("Please input row,col : ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            count++;
            if (checkWin(table, currentPlayer, col, row)) {
                finish = true;
                showTable();
                System.out.println(">>> " + currentPlayer + " Win <<<");
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showTable();
                System.out.println(">>> Draw <<<");
                return;
            }
            switchPlayer();
        }
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char table[][], char currentPlayer, int col, int row) {
        if (checkVertical(table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(table, currentPlayer, row)) {
            return true;
        } else if (checkDiagonal(table,currentPlayer)) {
            return true;
        }
        return false;
    }


   public static boolean checkVertical(char table[][], char currentPlayer, int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char table[][], char currentPlayer, int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDiagonal(char table[][], char currentPlayer) {
        if (checkDiagonal1(table,currentPlayer)) {
            return true;
        } else if (checkDiagonal2(table,currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonal1(char table[][], char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDiagonal2(char table[][], char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(int count) {
        if (count==9) {
            return true;
        }
        return false;
    }
}
