/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.projectox;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Ow
 */
public class OXTest {

    public OXTest() {
    }

    @Test
    public void testCheckVerticalPlayerOCol1() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', '-', '-'},
                {'O', '-', '-'},
                {'O', '-', '-'}};

        int col = 1;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', 'O', '-'},
                {'-', 'O', '-'},
                {'-', 'O', '-'}};
        int col = 2;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol3() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', 'O'},
                {'-', '-', 'O'},
                {'-', '-', 'O'}};
        int col = 3;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    // testCheckVerticalPlayerO "False"
    @Test
    public void testCheckVerticalPlayerOCol1False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', '-', '-'},
                {'O', '-', '-'},
                {'-', 'O', '-'}};
        int col = 1;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', 'O', '-'},
                {'-', 'O', '-'},
                {'-', '-', 'O'}};
        int col = 2;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol3False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', 'O'},
                {'-', '-', 'O'},
                {'-', 'O', '-'}};
        int col = 3;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    //testCheckHorizontalPlayerO
    @Test
    public void testCheckHorizontalPlayerORow1() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', 'O', 'O'},
                {'-', '-', '-'},
                {'-', '-', '-'}};
        int row = 1;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', '-'},
                {'O', 'O', 'O'},
                {'-', '-', '-'}};
        int row = 2;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow3() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', '-'},
                {'-', '-', '-'},
                {'O', 'O', 'O'}};
        int row = 3;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckHorizontalPlayerO Return "false"
    @Test
    public void testCheckHorizontalPlayerORow1False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', 'O', '-'},
                {'-', '-', 'O'},
                {'-', '-', '-'}};
        int row = 1;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', '-'},
                {'O', 'O', '-'},
                {'-', '-', 'O'}};
        int row = 2;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow3False() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', '-'},
                {'-', '-', 'O'},
                {'O', 'O', '-'}};
        int row = 3;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckXRightPlayerO
    @Test
    public void testCheckXRightPlayerO() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', '-', '-'},
                {'-', 'O', '-'},
                {'-', '-', 'O'}};
        assertEquals(true, OX.checkDiagonal1(table, currentPlayer));
    }

    //testCheckXLeftPlayerO
    @Test
    public void testCheckXLeftPlayerO() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', 'O'},
                {'-', 'O', '-'},
                {'O', '-', '-'}};
        assertEquals(true, OX.checkDiagonal2(table, currentPlayer));
    }

    //testCheckXLeftPlayerO False
    @Test
    public void testCheckXRightPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][]
                = {{'-', '-', 'O'},
                {'-', 'O', '-'},
                {'O', '-', '-'}};
        assertEquals(false, OX.checkDiagonal1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerOFalse() {
        char currentPlayer = 'O';
        char table[][]
                = {{'O', '-', '-'},
                {'-', 'O', '-'},
                {'-', '-', 'O'}};
        assertEquals(false, OX.checkDiagonal2(table, currentPlayer));
    }

    //testCheckDraw
    @Test
    public void testCheckDraw() {
        int count = 9;
        assertEquals(true, OX.checkDraw(count));
    }

    //CheckWinPlayerO
    @Test
    public void testCheckPlayerOWin() {
        char table[][]
                = {{'O', 'O', 'O'},
                {'-', '-', '-'},
                {'-', '-', '-'}};

        char currentPlayer = 'O';
        int col = 1;
        int row = 1;
        assertEquals(true, OX.checkWin(table, currentPlayer, col, row));
    }

    // testCheckVertical PlayerX
    @Test
    public void testCheckVerticalPlayerXCol1() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', '-', '-'},
                {'X', '-', '-'},
                {'X', '-', '-'}};
        int col = 1;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', 'X', '-'},
                {'-', 'X', '-'},
                {'-', 'X', '-'}};
        int col = 2;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol3() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', 'X'},
                {'-', '-', 'X'},
                {'-', '-', 'X'}};
        int col = 3;
        assertEquals(true, OX.checkVertical(table, currentPlayer, col));
    }

    // testCheckVerticalPlayerX "False"
    @Test
    public void testCheckVerticalPlayerXCol1False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', '-', '-'},
                {'X', '-', '-'},
                {'-', 'X', '-'}};
        int col = 1;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', 'X', '-'},
                {'-', 'X', '-'},
                {'-', '-', 'X'}};
        int col = 2;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol3False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', 'X'},
                {'-', '-', 'X'},
                {'-', 'X', '-'}};
        int col = 3;
        assertEquals(false, OX.checkVertical(table, currentPlayer, col));
    }

    //testCheckHorizontalPlayerX
    @Test
    public void testCheckHorizontalPlayerXRow1() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', 'X', 'X'},
                {'-', '-', '-'},
                {'-', '-', '-'}};
        int row = 1;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow2() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', '-'},
                {'X', 'X', 'X'},
                {'-', '-', '-'}};
        int row = 2;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow3() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', '-'},
                {'-', '-', '-'},
                {'X', 'X', 'X'}};
        int row = 3;
        assertEquals(true, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckHorizontalPlayerX Return "false"
    @Test
    public void testCheckHorizontalPlayerXRow1False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', 'X', '-'},
                {'-', '-', 'X'},
                {'-', '-', '-'}};
        int row = 1;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow2False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', '-'},
                {'X', 'X', '-'},
                {'-', '-', 'X'}};
        int row = 2;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow3False() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', '-'},
                {'-', '-', 'X'},
                {'X', 'X', '-'}};
        int row = 3;
        assertEquals(false, OX.checkHorizontal(table, currentPlayer, row));
    }

    //testCheckXLeftPlayerX
    @Test
    public void testCheckXRightPlayerX() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', '-', '-'},
                {'-', 'X', '-'},
                {'-', '-', 'X'}};
        assertEquals(true, OX.checkDiagonal1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerX() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', 'X'},
                {'-', 'X', '-'},
                {'X', '-', '-'}};
        assertEquals(true, OX.checkDiagonal2(table, currentPlayer));
    }
    
    //testCheckXLeftPlayerX False
    @Test
    public void testCheckXRightPlayerXFalse() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', 'X'},
                {'-', 'X', '-'},
                {'X', '-', '-'}};
        assertEquals(false, OX.checkDiagonal1(table, currentPlayer));
    }

    @Test
    public void testCheckXLeftPlayerXFalse() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', '-', '-'},
                {'-', 'X', '-'},
                {'-', '-', 'X'}};
        assertEquals(false, OX.checkDiagonal2(table, currentPlayer));
    }

    //testCheckXPlayerX
    @Test
    public void testCheckXRPlayerX() {
        char currentPlayer = 'X';
        char table[][]
                = {{'X', '-', '-'},
                {'-', 'X', '-'},
                {'-', '-', 'X'}};
        assertEquals(true, OX.checkDiagonal(table, currentPlayer));
    }

    @Test
    public void testCheckXPlayerX() {
        char currentPlayer = 'X';
        char table[][]
                = {{'-', '-', 'X'},
                {'-', 'X', '-'},
                {'X', '-', '-'}};
        assertEquals(true, OX.checkDiagonal(table, currentPlayer));
    }

    //CheckWinPlayerX
    @Test
    public void testCheckPlayerXWin() {

        char table[][]
                = {{'X', 'X', 'X'},
                {'-', '-', '-'},
                {'-', '-', '-'}};

        char currentPlayer = 'X';
        int col = 1;
        int row = 1;
        assertEquals(true, OX.checkWin(table, currentPlayer, col, row));
    }
   

   

}
